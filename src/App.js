import React, {useState} from 'react';


const App = () => {
  const [inputValue, setInputValue] = useState('')

  const onChange = e => { const value = e.target.value;
    setInputValue(value)
    document.title = value
  }

  return <input value={inputValue} onChange={onChange}/>
};

export default App;

